import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
FormsModule,
ReactiveFormsModule
} from '@angular/forms';
import {
MatTableModule,
MatButtonModule, 
MatCheckboxModule,
MatToolbarModule,
MatInputModule,
MatFormFieldModule,
MatCardModule,
MatProgressSpinnerModule,
MatPaginatorModule,
MatSortModule,
MatIconModule,
MatMenuModule,
MatIconRegistry
} from '@angular/material';
import { AppRoutingModule } from './app-routing-module';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { PersonsComponent } from './persons/persons.component';
import { SunwingDataService } from './sunwing-data-service.service';
import { MessagesService } from './messages.service';
import { HttpModule } from '@angular/http';
import { MessagesComponent } from './messages/messages.component';


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    PersonsComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatToolbarModule,
    MatInputModule,
    MatCardModule,
    HttpClientModule,
    MatTableModule,
    HttpModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatMenuModule
      ],
  providers: [SunwingDataService,MessagesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
