import boto3
import os
import uuid
import re
from pprint import pprint
import json

def validation(input,inputType):
    regex = {
        'email': r'/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/',
        'phone': r'/^[1-9][0-9]{6,11}$/',
        'date': r'/(^(((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/'
    }[inputType]
    print('Trying to match' + input + ' as ' + inputType + ' with ' + regex)
    matchObj = re.search( regex, input, re.M|re.I)
    if matchObj:
       print "match --> matchObj.group() : ", matchObj.group()
    else:
       print "No match!!"
    
def lambda_handler(event, context):
    raise Exception('Malformed input ...')

def lambda_handler(event, context):

    myjson = {}

    if 'id' in event:
        myjson["id"] = event["id"]
    else:
        myjson["id"] = str(uuid.uuid4())

    if 'phone' in event:
        if event["phone"]:
            myjson["phone"] = event["phone"]
            print('phone')

    if 'birthday' in event:
        if event["birthday"]:
            myjson["birthday"] = event["birthday"]
            print('bday')

    if 'type' in event:
        if event["type"]:
            myjson["type"] =  event["type"]
            print('type')

    if 'name' in event:
        if event["name"]:
            myjson["name"] = event["name"]
            print('name')

    if 'email' in event:
        if event["email"]:
            myjson["email"] = event["email"]
            print('email')


    json_data = json.dumps(myjson)
    
    print(json_data)
    
    #pprint(json_data)

    print('Generating new DynamoDB record, with: ')
    
    #Creating new record in DynamoDB table
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(os.environ['DB_TABLE_NAME'])
    table.put_item(Item=myjson)
    
    #Sending notification about new post to SNS
    client = boto3.client('sns')
    client.publish(
        TopicArn = os.environ['SNS_TOPIC'],
        Message = myjson["id"]
    )
    
    output = { "id": myjson["id"] , "status": "COMPLETED"}
    
    pprint(output);
    
    return output