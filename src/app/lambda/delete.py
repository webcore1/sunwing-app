from __future__ import print_function # Python 2/3 compatibility
import os
import boto3
from botocore.exceptions import ClientError
import json
import decimal

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

def lambda_handler(event, context):

    id = event["id"]
    
    if id == "*":
        reply = {"status":"INVALID"};
        return reply
    
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(os.environ['DB_TABLE_NAME'])
    

    try:
        response = table.delete_item(
            Key={
                'id': id 
            }
        )
    
    except ClientError as e:
        if e.response['Error']['Code'] == "ConditionalCheckFailedException":
            return(e.response['Error']['Message'])
        else:
            raise
    else:
        reply = {"status":"COMPLETED"};
        return reply
        #return(json.dumps(response))