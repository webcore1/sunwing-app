export class Person {
  id?: string;
  type: string;
  name: string;
  email: string;
  phone: number;
  birthday: string;
  status?: string;
}