import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { PersonsComponent } from './persons/persons.component';


const routes: Routes = [


{ path: '', redirectTo: '/signup', pathMatch: 'full' },
{ path: 'signup', component: SignupComponent },
{ path: 'signup/customer', component: SignupComponent, data: { person: 'customer' } },
{ path: 'signup/supplier', component: SignupComponent, data: { person: 'supplier'  } },
{ path: 'persons', component: PersonsComponent },
{ path: 'edit/:id', component: SignupComponent }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { enableTracing: false }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/