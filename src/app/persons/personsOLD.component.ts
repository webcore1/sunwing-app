import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SunwingDataService } from '../sunwing-data-service.service';
import { Person } from '../person';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PersonsComponent implements OnInit {

  constructor(private dataService: SunwingDataService) { }

  persons : Person;

	getAllPersons(): void {

		this.dataService.getPersons('*')
		  .subscribe(persons => {


		  	this.persons = persons;

		  	console.log(persons);

		  } ,
		  	err => console.error(err), 
		  	() => {console.log('test2',this.persons); 
		  		
		  	}
		  	);

		  //this.job_text = this.job.text;

	}


  ngOnInit() {

  	this.getAllPersons();


  }

}
