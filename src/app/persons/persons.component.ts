import {Component, OnInit, ViewChild,ViewEncapsulation} from '@angular/core';
import {Http} from '@angular/http';
import {DataSource} from '@angular/cdk/collections';
import {MatPaginator, MatSort} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { SunwingDataService } from '../sunwing-data-service.service';
import { Person } from '../person';
import { MessagesService } from '../messages.service';


/**
 * @title Table retrieving data through HTTP
 */

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class PersonsComponent implements OnInit {
  displayedColumns = ['name' ,'type','email','phone','birthday','id'];
  swDatabase: GetAllPersons | null;
  dataSource: ExampleDataSource | null;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
  	private http: Http,
  	private dataService: SunwingDataService,
    private messagesService: MessagesService,
  	private router: Router
  ) {}

  edit(id) {
  	this.router.navigateByUrl('/edit/'+id);
  }

  openDeleteDialog(id) {
  	alert('id'+id);
  }



  delete(id): void{
        //get person for editing
  this.dataService.deletePerson(id)
    .subscribe(person => {

        console.log(person.status);

        if(person.status=="COMPLETED"){

          this.messagesService.add('Person Deleted!');

          //console.log(this);


              this.dataSource = new ExampleDataSource(
      this.swDatabase!, this.paginator, this.sort);


        }

    } ,
      err => console.error(err), 
        () => {console.log('test2' ); 
        
      }
      );
}



  ngOnInit() {
    this.swDatabase = new GetAllPersons(this.http);
    this.dataSource = new ExampleDataSource(
      this.swDatabase!, this.paginator, this.sort);
  }
}

export interface SWapi {
	items: SWpersons[];
	total_count: number;
}

export interface SWpersons {
  id: string;
  type: string;
  name: string;
  email: string;
  phone: number;
  birthday: string;
}

/** An example database that the data source uses to retrieve data for the table. */
export class GetAllPersons {
  constructor(private http: Http) {}

  getSWpersons(sort: string, order: string, page: number): Observable<SWpersons[]> {
    const href = 'https:\/\/mn2fmkkm5g.execute-api.us-east-1.amazonaws.com/dev';
    const requestUrl =
      `${href}?id=*&sort=${sort}&order=${order}&page=${page + 1}`;

    return this.http.get(requestUrl)
                    .map(response => response.json() as SWpersons[]);
  }
}




/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, GetAllPersons. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class ExampleDataSource extends DataSource<SWpersons> {
  // The number of issues returned by github matching the query.
  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = false;

  constructor(private swDatabase: GetAllPersons,
              private paginator: MatPaginator,
              private sort: MatSort) {
    super();
  }




  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<SWpersons[]> {
    const displayDataChanges = [
      this.sort.sortChange,
      this.paginator.page
    ];

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    return Observable.merge(...displayDataChanges)
      .startWith(null)
      .switchMap(() => {
        this.isLoadingResults = true;
        return this.swDatabase.getSWpersons(
          this.sort.active, this.sort.direction, this.paginator.pageIndex);
      })
      .map(data => {
        // Flip flag to show that loading has finished.
        console.log(data);
        this.isLoadingResults = false;
        //this.isRateLimitReached = false;
        this.resultsLength = data.length;

        return data;
      })
      .catch(() => {
        this.isLoadingResults = false;
        // Catch if the GitHub API has reached its rate limit. Return empty data.
        this.isRateLimitReached = true;
        return Observable.of([]);
      });
  }

  disconnect() {}
}