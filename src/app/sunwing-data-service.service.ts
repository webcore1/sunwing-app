import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Person } from './person';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class SunwingDataService {


private API_URI = 'https:\/\/mn2fmkkm5g.execute-api.us-east-1.amazonaws.com/dev'; 
 
constructor(private http: HttpClient) { }


    /** GET job by id. Return `undefined` when id not found */
  getPersons<Data>(id: string): Observable<Person> {
    const url = `${this.API_URI}/?id=${id}`;
    return this.http.get<Person>(url)
      .pipe(
        map(person => person[0]), // returns a {0|1} element array
        tap(h => {
          console.log('h',h);
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} person id=${id}`);
        }),
        catchError(this.handleError<Person>(`getperson1 id=${id}`))
      );
  }

    /** GET job by id. Return `undefined` when id not found */
  getJob<Data>(id: string): Observable<Person> {
    //const url = `${this.API_URI}?postId=${id}`;
    const url = "";
    return this.http.get<Person>(url)
      .pipe(
        map(person => person[0]), // returns a {0|1} element array
        tap(h => {
          //console.log('h',h);
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} person id=${id}`);
        }),
        catchError(this.handleError<Person>(`getperson1 id=${id}`))
      );
  }
 
  /** POST: add a new person to the server */
  addperson (person: Person): Observable<Person> {
    console.log(person);
    return this.http.post<Person>(this.API_URI, person, httpOptions).pipe(
      tap((person: Person) => this.log(`added person w/ id=${person.id}`)),
      catchError(this.handleError<Person>('addperson'))
    );
  }

  /** PUT: add a new person to the server */
  editperson (person: Person): Observable<Person> {
    console.log(person);
    return this.http.put<Person>(this.API_URI, person, httpOptions).pipe(
      tap((person: Person) => this.log(`added person w/ id=${person.id}`)),
      catchError(this.handleError<Person>('addperson'))
    );
  }



  deletePerson (person: Person): Observable<Person> {

    console.log("here:",person);
    const id =  person.id;
     const url = `${this.API_URI}/?id=${id}`;
     

    return this.http.delete<Person>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted hero id=${id}`)),
      catchError(this.handleError<Person>('deleteHero'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    console.log('Job: ' + message);
  }


}




