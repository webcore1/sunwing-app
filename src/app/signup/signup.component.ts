import {Component,ViewEncapsulation,OnInit,Input} from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {Location} from '@angular/common';
import { Person } from '../person';
import { SunwingDataService } from '../sunwing-data-service.service';
import { MessagesService } from '../messages.service';

import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
  } from '@angular/forms';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PHONE_REGEX = /^[1-9][0-9]{6,11}$/;
const DATE_REGEX = /(^(((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/;
const NAME_REGEX = /^(?!.{51,})([a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð,.'-]+\s+[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð,.'-]+ ?)$/;


/**
 * @title Input Errors
 */
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  encapsulation: ViewEncapsulation.None,
   animations: [
    trigger('heroState', [
      state('in123', style({transform: 'translateX(0)'})),
	    transition('void => *', [
	      style({transform: 'translateX(-100%)'}),
	      animate(300)
	    ]),
	    transition('* => void', [
	      animate(300, style({transform: 'translateX(100%)'}))
	    ])
    ])
  ]
  
})

export class SignupComponent implements OnInit {

	type = null;
  edit = null;
  title = null;
  subtitle = null;
  formvalid = false;
  person : Person;
  form: FormGroup;


	constructor(

  	private route: ActivatedRoute,
  	private location: Location,
    private dataService: SunwingDataService,
    private formBuilder: FormBuilder,
    private messagesService: MessagesService,
  	) { }


  emailFc = new FormControl('', [Validators.required, 
    Validators.pattern(EMAIL_REGEX)]);

  phoneFc = new FormControl('', [
    Validators.required,
    Validators.pattern(PHONE_REGEX)]);

  nameFc = new FormControl('', [
    Validators.required,
     Validators.pattern(NAME_REGEX)]);

    dateFc = new FormControl('', [
    Validators.pattern(DATE_REGEX)]);


    //initilize editing mode
    initEditing(id): void {

      console.log(id);

      //get person for editing
      this.dataService.getPersons(id)
        .subscribe(person => {

           this.type = person.type || null;
           //this.person.id = id;
           this.subtitle = "Edit " + this.type;

           this.emailFc.reset(person.email);
           this.phoneFc.reset(person.phone);
           this.nameFc.reset(person.name);
           this.dateFc.reset(person.birthday);

        } ,
          err => console.error(err), 
            () => {console.log('test2', this.type ); 

            
          }
          );

    }


    savePerson(data): void{


      //get person for editing
      this.dataService.addperson(data)
        .subscribe(person => {

            if(person.status=="COMPLETED"){
              this.messagesService.add('New Person Added');
            }
            
        } ,
          err => console.error(err), 
            () => {console.log('test2', this.type ); 
            
          }
          );
    }


    editPerson(data): void{


      //get person for editing
      this.dataService.editperson(data)
        .subscribe(person => {

            if(person.status=="COMPLETED"){
              this.messagesService.add('Person Edited');
            }
            
        } ,
          err => console.error(err), 
            () => {console.log('test2', this.type ); 
            
          }
          );
    }

  onSubmit(user): void{

console.log("submit",user);

    console.log(this.person);
    
    this.person = { 
    
      birthday: this.dateFc.value,
      phone: this.phoneFc.value,
      email: this.emailFc.value,
      type: this.type,
      name: this.nameFc.value

     };


    if(this.type=="customer" && 
      this.emailFc.valid && 
      this.nameFc.valid && 
      this.dateFc.valid){

      this.formvalid = true;

    }


     if(this.type=="supplier" && 
      this.phoneFc.valid && 
      this.nameFc.valid ){

       this.formvalid = true;

    }

    if(this.formvalid && !this.edit){
      this.savePerson(this.person);
    }else{
      this.person.id = this.edit;
      console.log('person for edit', this.person);
      this.editPerson(this.person);
    }

  }


   //on initialization
	ngOnInit(): void {

          console.log(this.route.snapshot);

          //check if type exists for new person
          var type = this.route.snapshot.data.person || null;
          if(type){
            this.type = type;
            this.title = "Singup";
            this.subtitle = "New " + type;
          }

          //check if id is being passed for editing
          var id = this.route.snapshot.params.id || null;
          if(id){
              this.initEditing(id);
              //this.person.id = id;
              this.edit = id;
              this.title = "Edit";
       
          }
		 
	}



	backClicked() {
        this.location.back();
    }

}