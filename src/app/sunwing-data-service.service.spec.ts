import { TestBed, inject } from '@angular/core/testing';

import { SunwingDataServiceService } from './sunwing-data-service.service';

describe('SunwingDataServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SunwingDataServiceService]
    });
  });

  it('should be created', inject([SunwingDataServiceService], (service: SunwingDataServiceService) => {
    expect(service).toBeTruthy();
  }));
});
